# Complete the fizzbuzz function to return
# * The word "fizzbuzz" if number is evenly divisible by
#   by both 3 and 5
# * The word "fizz" if number is evenly divisible by only
#   3
# * The word "buzz" if number is evenly divisible by only
#   5
# * The number if it is not evenly divisible by 3 nor 5
#
# Try to combine what you have done in the last two problems
# from memory.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def fizzbuzz(number):
    # if %3 and %5 return "fizzbuzz"
    if number % 3 == 0 and number % 5 == 0:
        return "Fizzbuzz"
    # if %3 "fizz"
    elif number % 3 == 0:
        return "Fizz"
    # if %5 "buzz"
    elif number % 5 == 0:
        return "Buzz"
    # num for all else
    else:
        return number

test = fizzbuzz(15)
test2 = fizzbuzz(9)
test3 = fizzbuzz(25)
test4 = fizzbuzz(7)
print(test, test2, test3, test4)
