# Complete the max_of_three function so that returns the
# maximum of three values.
#
# If two values are the same maximum value, return either of
# them.
# If the all of the values are the same, return any of them
#
# Use the >= operator for greater than or equal to

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.


def max_of_three(value1, value2, value3):
    # if value 1 greater than both
    if value1 >= value2 and value1 >= value3:
        return value1
    elif value2 >= value1 and value2 >= value3:
        return value2
    else:
        return value3

def max_of_three002(value1, value2, value3):
    return max(value1, value2, value3)


test1 = max_of_three(69,420,1)
test2 = max_of_three002(80085, 1, 9999999)
test3 = max_of_three(69420, 1, 69420)
test4 = max_of_three002(2,2,1)
