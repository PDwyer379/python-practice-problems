# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
test_list = [11, 22, 33, 44, 55, 66, 77, 88, 99]

def calculate_sum(values):
    sum = 0
    if len(values) == 0:
        return None
    else:
        # return sum(values)
        for i in values:
            sum += i
    return sum

test = calculate_sum(test_list)
print(test)
