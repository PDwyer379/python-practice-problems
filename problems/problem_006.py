# Complete the can_skydive function so that determines if
# someone can go skydiving based on these criteria
#
# * The person must be greater than or equal to 18 years old, or
# * The person must have a signed consent form

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_skydive(age, has_consent_form):
    #be old enough or has consent form
    has_consent_form = has_consent_form.lower()
    if age >= 18 or has_consent_form == "yes":
        return "You may go skydiving"
    else:
        return "You may not go skydiving"

test1 = can_skydive(16, "no")
test2 = can_skydive(18, "no")
test3 = can_skydive(1, "Yes")

print(test1, test2, test3)
