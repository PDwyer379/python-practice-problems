# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"

workdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"]

def gear_for_day(is_workday, is_sunny):

    # create empty list my_list for items
    # create list of workdays
  my_list = []
  if is_workday in workdays:
    my_list.append("Laptop")
  else:
    my_list.append("Surfboard")

  if not is_sunny:
    my_list.append("Umbrella")
  return my_list

test = print(gear_for_day('Monday', True))
test2 = print(gear_for_day('Tuesday', False))
test3 = print(gear_for_day('Saturday', True))

print(test, test2, test3)

# why am i getting None None None
