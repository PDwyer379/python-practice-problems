# Write a function that meets these requirements.
#
# Name:       temperature_differences
# Parameters: highs: a list of daily high temperatures
#             lows: a list of daily low temperatures
# Returns:    a new list containing the difference
#             between each high and low temperature
#
# The two lists will be the same length
#
# Example:
#     * inputs:  highs: [80, 81, 75, 80]
#                lows:  [72, 78, 70, 70]
#       result:         [ 8,  3,  5, 10]

def temp(list1, list2):
    new_list = []
    for i in range(0, len(list1)):
        new_list.append(list1[i] - list2[i])
    return new_list

print(temp([80, 81, 75, 80, 420], [72, 78, 70, 70, 69]))
