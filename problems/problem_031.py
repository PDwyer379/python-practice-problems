# Complete the sum_of_squares function which accepts
# a list of numerical values and returns the sum of
# each item squared
#
# If the list of values is empty, the function should
# return None
#
# Examples:
#   * [] returns None
#   * [1, 2, 3] returns 1*1+2*2+3*3=14
#   * [-1, 0, 1] returns (-1)*(-1)+0*0+1*1=2
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.
test_list = [11, 22, 33, 44, 55, 66, 77, 88, 99, 100, 1, 2, 3, 4]

def sum_of_squares(values):
    new_list = []
    if len(values) == 0:
        return None
    # square elements in list fist, then sum them together
    for i in values:
        new_list.append(i**2)
        print(new_list)
    return sum(new_list)

test = sum_of_squares(test_list)
print(test)
