# Complete the count_letters_and_digits function which
# accepts a parameter s that contains a string and returns
# two values, the number of letters in the string and the
# number of digits in the string
#
# Examples:
#   * "" returns 0, 0
#   * "a" returns 1, 0
#   * "1" returns 0, 1
#   * "1a" returns 1, 1
#
# To test if a character c is a digit, you can use the
# c.isdigit() method to return True of False
#
# To test if a character c is a letter, you can use the
# c.isalpha() method to return True of False
#
# Remember that functions can return more than one value
# in Python. You just use a comma with the return, like
# this:
#      return value1, value2
#
# Do it without pseudocode, this time, from memory. Don't look
# at the last one you just wrote unless you really must.

test_string = "T1he qu2ick brown fox jumps ov33er the lazy dog and some other0 stuff5 for good mea4sure"

def count_letters_and_digits(s):
    char = 0
    digit = 0
    for i in s:
        if i.isdigit():
            digit += 1
        else:
            i.isalpha()
            char += 1
    return digit, char
test = count_letters_and_digits(test_string)
print(test)
