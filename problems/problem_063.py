# Write a function that meets these requirements.
#
# Name:       shift_letters
# Parameters: a string containing a single word
# Returns:    a new string with all letters replaced
#             by the next letter in the alphabet
#
# If the letter "Z" or "z" appear in the string, then
# they would get replaced by "A" or "a", respectively.
#
# Examples:
#     * inputs:  "import"
#       result:  "jnqpsu"
#     * inputs:  "ABBA"
#       result:  "BCCB"
#     * inputs:  "Kala"
#       result:  "Lbmb"
#     * inputs:  "zap"
#       result:  "abq"
#
# You may want to look at the built-in Python functions
# "ord" and "chr" for this problem
def tough_one(string):
    empty_list = []
    plus1 = []
    new_string = ""
    z_to_a_string = ""
    for i in string:
        if i == "z":
            z_to_a_string += 'a'
        elif i == "Z":
            z_to_a_string += "A"
        else:
            z_to_a_string += i
    for i in z_to_a_string:
        empty_list.append(ord(i))
    print(empty_list)
    for i in empty_list:
        plus1.append(i + 1)
    print(plus1)
    for i in plus1:
        new_string += str(chr(i))
    print(new_string)


print(tough_one("FuckYaThisIsAFunOnezZ"))
