# Write four classes that meet these requirements.
#
# Name:       Animal
#
# Required state:
#    * number_of_legs, the number of legs the animal has
#    * primary_color, the primary color of the animal
#
# Behavior:
#    * describe()       # Returns a string that describes that animal
#                         in the format
#                                self.__class__.__name__
#                                + " has "
#                                + str(self.number_of_legs)
#                                + " legs and is primarily "
#                                + self.primary_color
class Animal:
    def __init__(self, num_legs, primary_color) -> None:
        self.num_legs = num_legs
        self.primary_color = primary_color

    def describe(self):
        # return(f"{Animal} has {num_legs} and is {primary_color}")
        return self.__class__.__name__ + " has " + str(self.number_of_legs) + " legs and is primarily " + self.primary_color

class Dog(Animal):
    def __init__(self, num_legs, primary_color) -> None:
        super().__init__(num_legs, primary_color)

    def speak(self):
        return "Bark!"

class Cat(Animal):
    def __init__(self, num_legs, primary_color) -> None:
        super().__init__(num_legs, primary_color)

    def speak(self):
        return "Miao!"

class Snake(Animal):
    def __init__(self, num_legs, primary_color) -> None:
        super().__init__(num_legs, primary_color)
        print()

    def speak(self):
        return "Sssssssssss!"


dog = Dog(4, "brown")
print(dog.describe())  # "Dog has 4 legs and is primarily brown"
print(dog.speak())     # "Bark!"

cat = Cat(4, "black")
print(cat.describe())  # "Cat has 4 legs and is primarily black"
print(cat.speak())     # "Miao!"

snake = Snake(0, "green")
print(snake.describe())  # "Snake has 0 legs and is primarily green"
print(snake.speak())     # "Sssssssssss!"

# Name:       Dog, inherits from Animal
#
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Bark!"
#
#
#
# Name:       Cat, inherits from Animal
#
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Miao!"
#
#
#
# Name:       Snake, inherits from Animal
#
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Sssssss!"
