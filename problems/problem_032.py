# Complete the sum_of_first_n_numbers function which accepts
# a numerical limit and returns the sum of the numbers from
# 0 up to and including limit.
#
# If the value of the limit is less than 0, then it should
# return None
#
# Examples:
#   * -1 returns None
#   * 0 returns 0
#   * 1 returns 0+1=1
#   * 2 returns 0+1+2=3
#   * 5 returns 0+1+2+3+4+5=15
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.
# test_list = [11, 22, 33, 44, 55, 66, 77, 88, 99, 100, 1, 2, 3, 4]

def sum_of_first_n_numbers(limit):
    sum = 0
    if limit < 0:
        return None
    # iterate over list up to value
    for i in range(limit+1):
        sum += i
    return sum

test = sum_of_first_n_numbers(5)
print(test)
