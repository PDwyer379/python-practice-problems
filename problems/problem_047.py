# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    value = False
    if len(password) >= 6 and len(password) <= 12:
        if not any(i.islower() for i in password):
            value = False
        if not any(i.isupper() for i in password):
            value = False
        if not any(i.isdigit() for i in password):
            value = False
        if not any(i in "$!@" for i in password):
            value = False
        else:
            value = True
    return value

print(check_password("password"))
print(check_password("Connan The Barbarian"))
print(check_password("SyS379@22h"))
