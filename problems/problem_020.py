# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

def has_quorum(attendees_list, members_list):
    #  if attendees > 50% of members quarm
    if attendees_list >= members_list/2:
        return "Quorum"
    else:
        return "No quorum"

test = has_quorum(62483589243454345, 2738945723490857234520375902349582570243)
test2 = has_quorum(3, 5)
test3 = has_quorum(5, 3)
test4 = has_quorum(50, 100)
print(test, test2,test3,test4)
