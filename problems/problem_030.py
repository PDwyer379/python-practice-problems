# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.
test_list = [11, 22, 33, 44, 55, 66, 77, 88, 99, 100, 1, 2, 3, 4]

def find_second_largest(values):
    # sort the list and return length -2
    values.sort()
    return values[len(values)-2]

test = find_second_largest(test_list)
print(test)
