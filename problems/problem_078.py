# Write a class that meets these requirements.
#
# Name:       Circle
#
# Required state:
#    * radius, a non-negative value
#
# Behavior:
#    * calculate_perimeter()  # Returns the length of the perimater of the circle
#    * calculate_area()       # Returns the area of the circle
#
import math

class Circle:
    def __init__(self, radius) -> None:
        if radius > 0:
            self.radius = radius
        else:
            self.radius = None

    def calculate_perimeter(self):
        self.perimeter = 2*math.pi*self.radius
        return self.perimeter

    def calculate_area(self):
        self.area = math.pi*self.radius **2
        return self.area

test = Circle(1000)

print(test.calculate_area())
print(test.calculate_perimeter())

test = Circle(0)
print(test.calculate_area())
print(test.calculate_perimeter())












# Example:
#    circle = Circle(10)
#
#    print(circle.calculate_perimeter())  # Prints 62.83185307179586
#    print(circle.calculate_area())       # Prints 314.1592653589793
#
# Do it without pseudocode, this time, from memory. Don't look
# at the last one you just wrote unless you really must.
