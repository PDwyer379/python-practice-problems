# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.
test_string = "The quick brown fox jumps over the lazy dog or something like that"
def remove_duplicate_letters(s):
    list = ""
    for i in s:
        if i not in list:
            list += i

    return(list)


test = remove_duplicate_letters(test_string)
print(test)
