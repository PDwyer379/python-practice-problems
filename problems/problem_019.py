# Complete the is_inside_bounds function which has the
# following parameters:
#   x: the x coordinate to check
#   y: the y coordinate to check
#   rect_x: The left of the rectangle
#   rect_y: The bottom of the rectangle
#   rect_width: The width of the rectangle
#   rect_height: The height of the rectangle
#
# The is_inside_bounds function returns true if all of
# the following are true
#   * x is greater than or equal to rect_x
#   * y is greater than or equal to rect_y
#   * x is less than or equal to rect_x + rect_width
#   * y is less than or equal to rect_y + rect_height

def is_inside_bounds(x, y, rect_x, rect_y, rect_width, rect_height):
    # x and y checks then
    if x >= rect_x and y >= rect_y and x <= rect_x + rect_width and y <= rect_y + rect_height:
        return True
    else:
        return False


test = is_inside_bounds(9, 9, 8, 8, 11, 11)

def is_inside_bounds(x, y, rect_x, rect_y, rect_width, rect_height):
    if (                                                # solution
        x >= rect_x                                     # solution
        and y >= rect_y                                 # solution
        and x <= rect_x + rect_width                    # solution
        and y <= rect_y + rect_height                   # solution
    ):                                                  # solution
        return True                                     # solution
    else:                                               # solution
        return False                                    # solution
    # pass                                              # problem
