# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]

import math


def halve(list):
    half = math.ceil(len(list)/2)
    half2 = len(list) - half
    length = len(list) + 1
    list1 = list[0:half]
    list2 = list[half: length]
    return list1, list2, half, half2


print(halve([1,2,3,4,5,6,7,8,9,]))
